const AccountModel = (sequelize, DataTypes) => {
  const accounts = sequelize.define('accounts', {
    balance: DataTypes.DECIMAL(10, 2)
  })

  accounts.associate = ({ users }) => {
    accounts.belongsTo(users)
  }

  return accounts
}

module.exports = AccountModel