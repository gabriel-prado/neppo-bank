const SaleCreditModel = (sequelize, DataTypes) => {
  const salesCredit = sequelize.define('sales_credit', {
    description: DataTypes.STRING,
    value: DataTypes.DECIMAL(10, 2),
    isPaid: DataTypes.BOOLEAN
  }, { freezeTableName: true })

  salesCredit.associate = ({ credit_cards }) => {
    salesCredit.belongsTo(credit_cards, { as: 'creditCard' })
  }

  return salesCredit
}

module.exports = SaleCreditModel