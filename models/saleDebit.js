const SaleDebitModel = (sequelize, DataTypes) => {
  const salesDebit = sequelize.define('sales_debit', {
    description: DataTypes.STRING,
    value: DataTypes.DECIMAL(10, 2)
  }, { freezeTableName: true })

  salesDebit.associate = ({ debit_cards }) => {
    salesDebit.belongsTo(debit_cards, { as: 'debitCard' })
  }

  return salesDebit
}

module.exports = SaleDebitModel