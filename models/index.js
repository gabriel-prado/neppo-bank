const Sequelize = require('sequelize')

const {
  DB_DATABASE,
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_CONNECTION
} = process.env;

const sequelize = new Sequelize(
  DB_DATABASE,
  DB_USER,
  DB_PASSWORD, {
    dialect: DB_CONNECTION,
    host: DB_HOST,
    logging: false
  }
)

const models = {}
const fs = require('fs')
const path = require('path')
fs
  .readdirSync(__dirname)
  .filter((file) => 'index.js' !== file)
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file))
    models[model.name] = model
  })

Object.keys(models).forEach(modelName => {
  if ('associate' in models[modelName]) {
    models[modelName].associate(models)
  }
})

module.exports = {
  sequelize,
  models,
}