const DebitCardModel = (sequelize, DataTypes) => {
  const debitCards = sequelize.define('debit_cards', {
    newCardStatus: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN
  })

  debitCards.associate = ({ accounts }) => {
    debitCards.belongsTo(accounts)
  }

  return debitCards
}

module.exports = DebitCardModel