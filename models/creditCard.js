const DebitCardModel = (sequelize, DataTypes) => {
  const creditCard = sequelize.define('credit_cards', {
    isActive: DataTypes.BOOLEAN,
    limit: DataTypes.DECIMAL(10, 2),
    balance: DataTypes.DECIMAL(10, 2),
    expirationDate: DataTypes.INTEGER
  })

  creditCard.associate = ({ accounts, credit_cards }) => {
    creditCard.belongsTo(accounts)
    creditCard.belongsTo(credit_cards, { as: 'aditionalCard' })
  }

  return creditCard
}

module.exports = DebitCardModel