const StatementModel = (sequelize, DataTypes) => {
  const statements = sequelize.define('statements', {
    description: DataTypes.STRING,
    value: DataTypes.DECIMAL(10, 2),
    isCommitted: DataTypes.BOOLEAN,
    isScheduling: DataTypes.BOOLEAN
  })

  statements.associate = ({ accounts }) => {
    statements.belongsTo(accounts)
  }

  return statements
}

module.exports = StatementModel