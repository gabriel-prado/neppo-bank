const { MessageFactory, InputHints } = require('botbuilder');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const WATERFALL_DIALOG = 'waterfallDialog';
const menuString =
  'Navegue através do menu digitando os seguintes números:\n\n' +
  '1 - Conta corrente\n\n' +
  '2 - Cartões\n\n' +
  '3 - Demonstrativos\n\n' +
  '4 - Provisionamento de saque\n\n' +
  '5 - Dados Pessoais\n\n'
const { CancelAndHelpDialog } = require('./cancelAndHelpDialog');

class MenuDialog extends CancelAndHelpDialog {
  constructor() {
    super('MenuDialog');

    this
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.printMenuStep.bind(this)
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async printMenuStep(stepContext) {
    return await stepContext.context.sendActivity(menuString);
  }
}

module.exports.MenuDialog = MenuDialog;