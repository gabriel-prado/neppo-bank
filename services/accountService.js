const { UserService } = require('./userService')
const { models } = require('../models/index')

class AccountService {

  async balance(userId) {
    const userService = new UserService()
    const user = await userService.getById(userId)
    const account = await models.accounts.findAll({
      limit: 1,
      where: {
        userId: user.id
      }
    })

    return `O seu saldo é: ${account[0].balance}`
  }

}

module.exports.AccountService = AccountService;