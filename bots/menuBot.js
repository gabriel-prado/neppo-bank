const { ActivityHandler } = require('botbuilder');
const { models } = require('../models/index')

class MenuBot extends ActivityHandler {
  constructor(conversationState, dialog) {
    super();
    this.dialog = dialog
    this.conversationState = conversationState;
    this.dialogState = this.conversationState.createProperty('DialogState');

    this.onMembersAdded(async (context, next) => {
      const membersAdded = context.activity.membersAdded;
      for (let cnt = 0; cnt < membersAdded.length; ++cnt) {
        if (membersAdded[cnt].id !== context.activity.recipient.id) {
          const user = await this.getUser(membersAdded[cnt].id)

          if (!!user) {
            await context.sendActivity(`Olá ${user.name}, seja bem-vindo ao chat da Neppo Bank!`);
            await dialog.run(context, conversationState.createProperty('DialogState'), user.id);
          } else {
            await context.sendActivity(`Usuário não localizado! Tente novamente com outro id. (id: ${membersAdded[cnt].id})`);
          }
        }
      }

      await next();
    });

    this.onMessage(async (context, next) => {
      const message = context.activity.text
      const member = context.activity.from;
      const user = await this.getUser(member.id)

      if (!!user) {
        await this.dialog.run(context, this.dialogState, user.id);
      } else {
        await context.sendActivity(`Usuário não localizado! Tente novamente com outro id. (id: ${member.id})`);
      }

      await next();
    });

    this.onDialog(async (context, next) => {
      await this.conversationState.saveChanges(context, false);

      await next();
    });
  }

  async getUser(userId) {
    return !!userId && !isNaN(userId) ? await models.users.findByPk(userId) : null
  }

}

module.exports.MenuBot = MenuBot;