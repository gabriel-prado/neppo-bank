const moment = require('moment')

module.exports = async ({ accounts, credit_cards, debit_cards, sales_credit, sales_debit, statements, users }) => {
  //usuário
  await users.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      name: 'Gabriel',
      email: 'gabriel.prado.profeta@gmail.com',
      phone: '34996906055',
      address: 'Rua Quinze de Novembro 112'
    }
  })

  //conta corrente
  await accounts.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      balance: 1000.00,
      userId: 1
    }
  })

  //cartão de débito
  await debit_cards.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      accountId: 1,
      isActive: true
    }
  })

  //cartão de crédito adicional
  await credit_cards.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      accountId: 1,
      isActive: true,
      limit: 900.00,
      balance: 860.00,
      expirationDate: 15
    }
  })

  //cartão de crédito principal
  await credit_cards.findOrCreate({
    where: {
      id: 2
    },
    defaults: {
      id: 2,
      accountId: 1,
      isActive: true,
      limit: 2000.00,
      balance: 1200.00,
      aditionalCardId: 1,
      expirationDate: 20
    }
  })

  //venda cartão de crédito
  await sales_credit.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      description: 'supermercado',
      creditCardId: 1,
      value: 37.00,
      isPaid: false
    }
  })

  //venda cartão de débito
  await sales_debit.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      description: 'restaurante',
      debitCardId: 1,
      value: 58.00
    }
  })

  //extrato de um saque
  await statements.findOrCreate({
    where: {
      id: 1
    },
    defaults: {
      id: 1,
      description: 'saque no caixa',
      value: -300.00,
      isCommitted: true,
      isScheduling: false,
      accountId: 1
    }
  })

  //extrato de um deposito
  await statements.findOrCreate({
    where: {
      id: 2
    },
    defaults: {
      id: 2,
      description: 'deposito',
      value: 400.00,
      isCommitted: true,
      isScheduling: false,
      accountId: 1
    }
  })

  //lançamento futuro
  await statements.findOrCreate({
    where: {
      id: 3
    },
    defaults: {
      id: 3,
      description: 'recebimento de salário',
      value: 1100.00,
      isCommitted: false,
      isScheduling: false,
      accountId: 1
    }
  })

  //saque agendado
  await statements.findOrCreate({
    where: {
      id: 4
    },
    defaults: {
      id: 4,
      description: 'saque agendado',
      value: 10000.00,
      isCommitted: false,
      isScheduling: true,
      accountId: 1
    }
  })

  //extrato de um deposito antigo
  await statements.findOrCreate({
    where: {
      id: 5
    },
    defaults: {
      id: 5,
      description: 'deposito antigo',
      value: 750.00,
      isCommitted: true,
      isScheduling: false,
      accountId: 1,
      createdAt: moment().subtract(3, 'months')
    }
  })

  //venda cartão de débito antiga
  await sales_debit.findOrCreate({
    where: {
      id: 2
    },
    defaults: {
      id: 2,
      description: 'venda antiga',
      debitCardId: 1,
      value: 100.00,
      createdAt: moment().subtract(1, 'month')
    }
  })
}