const { InputHints, MessageFactory } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { models } = require('../models/index')
const moment = require('moment')
const { Op } = require("sequelize");

const TEXT_PROMPT = 'TextPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const MENU_ACCOUNT =
  'Navegue através do menu digitando os seguintes números:\n\n' +
  '1 - Consultar\n\n' +
  '2 - Agendar\n\n' +
  '3 - Excluir\n\n' +
  '0 - Menu Principal\n\n'

class ProvisioningDialog extends ComponentDialog {
  constructor() {
    super('ProvisioningDialog');

    this
      .addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.actStep.bind(this),
        this.list.bind(this),
        this.createInit.bind(this),
        this.createFinal.bind(this),
        this.deleteInit.bind(this),
        this.deleteFinal.bind(this),
        this.finalStep.bind(this),
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!this.userId) this.userId = menuDetails.userId

    const account = await models.accounts.findOne({
      where: {
        userId: this.userId,
      }
    })

    const statements = await models.statements.findAll({
      where: {
        accountId: account.id,
        isScheduling: true
      }
    })

    stepContext.options.account = account
    stepContext.options.statements = statements

    if (!menuDetails.secondChoice) {
      const msg = MessageFactory.text(MENU_ACCOUNT, MENU_ACCOUNT, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next();
  }

  async actStep(stepContext) {
    const menuDetails = stepContext.options;
    menuDetails.secondChoice = stepContext.result

    const num = menuDetails.secondChoice;

    if (isNaN(num) || (parseInt(num) > 3 || parseInt(num) < 0)) {
      await stepContext.context.sendActivity('Escolha um número válido!')
    }

    return await stepContext.next(menuDetails)
  }

  async list(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "1") {
      await _printProvisioning(stepContext, menuDetails.statements)
    }

    return await stepContext.next(menuDetails)
  }

  async createInit(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "2") {
      const message = 'Insira o valor do provisionamento: '
      const msg = MessageFactory.text(message, message, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next(menuDetails)
  }

  async createFinal(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "2") {
      await models.statements.create({
        description: 'saque agendado',
        value: stepContext.result,
        isCommitted: false,
        isScheduling: true,
        accountId: menuDetails.account.id
      })
      await stepContext.context.sendActivity('Provisionamento cadastrado com sucesso!')
    }

    return await stepContext.next(menuDetails)
  }

  async deleteInit(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "3") {
      if (!!menuDetails.statements.length) {
        await _printProvisioning(stepContext, menuDetails.statements)
        const message = 'Insira o número do provisionamento desejado: '
        const msg = MessageFactory.text(message, message, InputHints.ExpectingInput);
        return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
      } else {
        await stepContext.context.sendActivity(`Você não possui provisionamentos!`)
      }
    }

    return await stepContext.next(menuDetails)
  }

  async deleteFinal(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "3" && !isNaN(stepContext.result)) {
      const num = stepContext.result
      const statementId = menuDetails.statements[num - 1].id
      const statement = await models.statements.findByPk(statementId)
      await statement.destroy()
      await stepContext.context.sendActivity('Provisionamento cancelado com sucesso!')
    }

    return await stepContext.next(menuDetails)
  }

  async finalStep(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "0") {
      return await stepContext.beginDialog('MainDialog', {})
    }

    return await stepContext.replaceDialog(this.initialDialogId)
  }

}

const _printProvisioning = async (stepContext, provisioning) => {
  let messages = ``

  provisioning.forEach(async (provisioning, i) => {
    messages += `${ i + 1 } - ${provisioning.description}: ${provisioning.value} \n\n`
  });

  if (!messages) messages = `Você não possui provisionamentos!`

  await stepContext.context.sendActivity(messages)
}

module.exports.ProvisioningDialog = ProvisioningDialog;