const { InputHints, MessageFactory } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { models } = require('../models/index')
const moment = require('moment')
const { Op } = require("sequelize");

const TEXT_PROMPT = 'TextPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const MENU_DEBIT =
  'Navegue através do menu digitando os seguintes números:\n\n' +
  '1 - Bloquear Cartão\n\n' +
  '2 - Desbloquear Cartão\n\n' +
  '3 - Rastrear Cartão\n\n' +
  '4 - Emitir 2ª via\n\n' +
  '5 - Extrato de Compras (Últimos 15 dias)\n\n' +
  '6 - Compras por Período\n\n' +
  '0 - Menu Principal\n\n'

class DebitCardDialog extends ComponentDialog {
  constructor() {
    super('DebitCardDialog');

    this
      .addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.actStep.bind(this),
        this.blockDebit.bind(this),
        this.unblockDebit.bind(this),
        this.trackCard.bind(this),
        this.newCard.bind(this),
        this.debitSales.bind(this),
        this.debitSalesByDateIntro.bind(this),
        this.debitSalesByDateFinal.bind(this),
        this.finalStep.bind(this),
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!this.userId) this.userId = menuDetails.userId

    const account = await models.accounts.findOne({
      where: {
        userId: this.userId
      }
    })

    const debitCard = await models.debit_cards.findOne({
      where: {
        accountId: account.id
      }
    })

    stepContext.options.debitCard = debitCard

    if (!menuDetails.secondChoice) {
      const msg = MessageFactory.text(MENU_DEBIT, MENU_DEBIT, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next();
  }

  async actStep(stepContext) {
    const menuDetails = stepContext.options;
    menuDetails.secondChoice = stepContext.result

    const num = menuDetails.secondChoice;

    if (isNaN(num) || (parseInt(num) > 6 || parseInt(num) < 0)) {
      await stepContext.context.sendActivity('Escolha um número válido!')
    }

    return await stepContext.next(menuDetails)
  }

  async blockDebit(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "1") {
      let message = ``
      if (menuDetails.debitCard.isActive) {
        await models.debit_cards.update({ isActive: false }, { where: { id: menuDetails.debitCard.id } })
        message = `O seu cartão foi bloquado com sucesso!`
      } else {
        message = `O seu cartão já está bloqueado!`
      }
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async unblockDebit(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "2") {
      let message = ``
      if (!menuDetails.debitCard.isActive) {
        await models.debit_cards.update({ isActive: true }, { where: { id: menuDetails.debitCard.id } })
        message = `O seu cartão foi desbloqueado com sucesso!`
      } else {
        message = `O seu cartão já está desbloqueado!`
      }
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async trackCard(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "3") {
      let message = ``
      if (menuDetails.debitCard.newCardStatus) {
        message = `O seu cartão está sendo preparado para envio!`
      } else {
        message = `Não foi solicitado um novo cartão!`
      }
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async newCard(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "4") {
      let message = ``
      if (!menuDetails.debitCard.newCardStatus) {
        await models.debit_cards.update({ newCardStatus: true }, { where: { id: menuDetails.debitCard.id } })
        message = `A segunda via do seu cartão foi solicitada com sucesso!`
      } else {
        message = `A segunda via do seu cartão já foi solicitada!`
      }
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async debitSales(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "5") {
      const where = {
        where: {
          debitCardId: menuDetails.debitCard.id,
          createdAt: {
            [Op.gte]: moment().subtract(15, 'days')
          }
        }
      }

      const salesDebitCard = await models.sales_debit.findAll(where)
      await _printSalesDebitCard(stepContext, salesDebitCard)
    }

    return await stepContext.next(menuDetails)
  }

  async debitSalesByDateIntro(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "6") {
      const message = 'Insira uma quantidade de dias para exibir suas compras: '
      const msg = MessageFactory.text(message, message, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next(menuDetails)
  }

  async debitSalesByDateFinal(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "6") {
      const days = stepContext.result
      const where = {
        where: {
          debitCardId: menuDetails.debitCard.id,
          createdAt: {
            [Op.gte]: moment().subtract(days, 'days')
          }
        }
      }

      const salesDebitCard = await models.sales_debit.findAll(where)
      await _printSalesDebitCard(stepContext, salesDebitCard)
    }

    return await stepContext.next(menuDetails)
  }

  async finalStep(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "0") {
      return await stepContext.beginDialog('MainDialog', {})
    }

    return await stepContext.replaceDialog(this.initialDialogId)
  }

}

const _printSalesDebitCard = async (stepContext, salesDebitCard) => {
  let messages = ``

  salesDebitCard.forEach(async (sale, i) => {
    messages += `${sale.description}: ${sale.value} \n\n`
  });

  if (!messages) messages = `Você não possui vendas nesse período!`

  await stepContext.context.sendActivity(messages)
}

module.exports.DebitCardDialog = DebitCardDialog;