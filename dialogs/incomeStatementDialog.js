const { InputHints, MessageFactory } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { models } = require('../models/index')
const moment = require('moment')
const { Op } = require("sequelize");

const TEXT_PROMPT = 'TextPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';

class IncomeStatementDialog extends ComponentDialog {
  constructor() {
    super('IncomeStatementDialog');

    this
      .addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.finalStep.bind(this)
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!this.userId) this.userId = menuDetails.userId

    const account = await models.accounts.findOne({
      where: {
        userId: this.userId,
      }
    })

    const statements = await models.statements.findAll({
      where: {
        accountId: account.id,
        createdAt: {
          [Op.gte]: moment().subtract(1, 'year')
        },
        isCommitted: true
      }
    })

    let message = `Esses são seus informes de rendimento:\n\n`

    statements.forEach(async (statement, i) => {
      message += `${statement.description}: ${statement.value} \n\n`
    })

    await stepContext.context.sendActivity(message)

    return await stepContext.next();
  }

  async finalStep(stepContext) {
    return await stepContext.beginDialog('MainDialog', {})
  }

}

module.exports.IncomeStatementDialog = IncomeStatementDialog;