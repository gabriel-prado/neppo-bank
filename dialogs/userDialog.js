const { InputHints, MessageFactory } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { models } = require('../models/index')
const moment = require('moment')
const { Op } = require("sequelize");

const TEXT_PROMPT = 'TextPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';

class UserDialog extends ComponentDialog {
  constructor() {
    super('UserDialog');

    this
      .addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.actStep.bind(this),
        this.update.bind(this)
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!this.userId) this.userId = menuDetails.userId

    const user = await models.users.findByPk(this.userId)

    stepContext.options.user = user

    let message = `Esses são os seus dados:\n\n` +
      `email: ${user.email}\n\n` +
      `telefone: ${user.phone}\n\n` +
      `endereço: ${user.address}\n\n` +
      `Qual deles você quer alterar?`

    const msg = MessageFactory.text(message, message, InputHints.ExpectingInput);
    return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
  }

  async actStep(stepContext) {
    const menuDetails = stepContext.options;
    const choice = stepContext.result.toLowerCase()
    stepContext.options.choice = choice
    const options = ['endereço', 'email', 'telefone']

    if (!options.includes(choice)) {
      await stepContext.context.sendActivity('Não entendi, tente novamente!')
      return await stepContext.replaceDialog(this.initialDialogId)
    }

    const message = `Me diga o seu novo ${choice}:`
    const msg = MessageFactory.text(message, message, InputHints.ExpectingInput);
    return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
  }

  async update(stepContext) {
    const menuDetails = stepContext.options;
    const choice = menuDetails.choice
    const result = stepContext.result;
    const keys = {
      'endereço': 'address',
      'email': 'email',
      'telefone': 'phone'
    }
    const param = {}
    param[keys[choice]] = result

    await models.users.update(param, { where: { id: menuDetails.user.id } })

    await stepContext.context.sendActivity('Seus dados foram atualizados com sucesso!')
    return await stepContext.beginDialog('MainDialog', {})
  }

}


module.exports.UserDialog = UserDialog;