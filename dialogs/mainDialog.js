const { MessageFactory, InputHints } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');

const { AccountDialog } = require('./accountDialog');
const { CreditCardDialog } = require('./creditCardDialog');
const { DebitCardDialog } = require('./debitCardDialog');
const { IncomeStatementDialog } = require('./incomeStatementDialog');
const { ProvisioningDialog } = require('./provisioningDialog');
const { UserDialog } = require('./userDialog');

const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';
const TEXT_PROMPT = 'TextPrompt';

const MENU =
  'Navegue através do menu digitando os seguintes números:\n\n' +
  '1 - Conta corrente\n\n' +
  '2 - Cartão de Crédito\n\n' +
  '3 - Cartão de Débito\n\n' +
  '4 - Demonstrativos\n\n' +
  '5 - Provisionamento de saque\n\n' +
  '6 - Dados Pessoais\n\n'

class MainDialog extends ComponentDialog {
  constructor() {
    super('MainDialog');

    this
      .addDialog(new TextPrompt('TextPrompt'))
      .addDialog(new AccountDialog())
      .addDialog(new CreditCardDialog())
      .addDialog(new DebitCardDialog())
      .addDialog(new IncomeStatementDialog())
      .addDialog(new ProvisioningDialog())
      .addDialog(new UserDialog())
      .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.actStep.bind(this)
      ]));

    this.initialDialogId = MAIN_WATERFALL_DIALOG;
  }

  async run(turnContext, accessor, userId) {
    this.userId = userId
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);
    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();

    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!menuDetails.firstChoice) {
      const msg = MessageFactory.text(MENU, MENU, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next();
  }

  async actStep(stepContext) {
    const menuDetails = stepContext.options;
    menuDetails.firstChoice = stepContext.result;
    menuDetails.userId = this.userId

    switch (menuDetails.firstChoice) {
      case "1":
        return await stepContext.beginDialog('AccountDialog', menuDetails)
        break;
      case "2":
        return await stepContext.beginDialog('CreditCardDialog', menuDetails)
        break;
      case "3":
        return await stepContext.beginDialog('DebitCardDialog', menuDetails)
        break;
      case "4":
        return await stepContext.beginDialog('IncomeStatementDialog', menuDetails)
        break;
      case "5":
        return await stepContext.beginDialog('ProvisioningDialog', menuDetails)
        break;
      case "6":
        return await stepContext.beginDialog('UserDialog', menuDetails)
        break;
      default:
        await stepContext.context.sendActivity('Escolha um número válido!')
        return await stepContext.replaceDialog(this.initialDialogId)
    }
  }

}

module.exports.MainDialog = MainDialog;