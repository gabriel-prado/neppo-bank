const { InputHints, MessageFactory } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { models } = require('../models/index')
const moment = require('moment')
const { Op } = require("sequelize");

const TEXT_PROMPT = 'TextPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const MENU_ACCOUNT =
  'Navegue através do menu digitando os seguintes números:\n\n' +
  '1 - Saldo\n\n' +
  '2 - Extrato\n\n' +
  '3 - Extrato por Período\n\n' +
  '4 - Lançamento Futuros\n\n' +
  '0 - Menu Principal\n\n'

class AccountDialog extends ComponentDialog {
  constructor() {
    super('AccountDialog');

    this
      .addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.actStep.bind(this),
        this.balance.bind(this),
        this.statement.bind(this),
        this.statementByDateIntro.bind(this),
        this.statementByDateFinal.bind(this),
        this.futureRelease.bind(this),
        this.finalStep.bind(this),
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!this.userId) this.userId = menuDetails.userId

    const account = await models.accounts.findOne({
      where: {
        userId: this.userId
      }
    })

    stepContext.options.account = account

    if (!menuDetails.secondChoice) {
      const msg = MessageFactory.text(MENU_ACCOUNT, MENU_ACCOUNT, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next();
  }

  async actStep(stepContext) {
    const menuDetails = stepContext.options;
    menuDetails.secondChoice = stepContext.result

    const num = menuDetails.secondChoice;

    if (isNaN(num) || (parseInt(num) > 4 || parseInt(num) < 0)) {
      await stepContext.context.sendActivity('Escolha um número válido!')
    }

    return await stepContext.next(menuDetails)
  }

  async balance(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "1") {
      await stepContext.context.sendActivity(`O seu saldo é ${menuDetails.account.balance}`)
    }

    return await stepContext.next(menuDetails)
  }

  async statement(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "2") {
      const statements = await models.statements.findAll({ where: { accountId: menuDetails.account.id } })
      await _printStatements(stepContext, statements)
    }

    return await stepContext.next(menuDetails)
  }

  async statementByDateIntro(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "3") {
      const message = 'Insira a quantidade de meses para o extrato: '
      const msg = MessageFactory.text(message, message, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next(menuDetails)
  }

  async statementByDateFinal(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "3") {
      const months = stepContext.result
      const where = {
        where: {
          accountId: menuDetails.account.id,
          createdAt: {
            [Op.gte]: moment().subtract(months, 'months')
          }
        }
      }

      const statements = await models.statements.findAll(where)
      await _printStatements(stepContext, statements)
    }

    return await stepContext.next(menuDetails)
  }

  async futureRelease(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "4") {
      const statements = await models.statements.findAll({
        where: {
          accountId: menuDetails.account.id,
          isCommitted: false,
          isScheduling: false
        }
      })
      if (!!statements.length)
        await stepContext.context.sendActivity(`Esses são seus lançamentos futuros:`)
      await _printStatements(stepContext, statements)
    }

    return await stepContext.next(menuDetails)
  }

  async finalStep(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "0") {
      return await stepContext.beginDialog('MainDialog', {})
    }

    return await stepContext.replaceDialog(this.initialDialogId)
  }

}

const _printStatements = async (stepContext, statements) => {
  let messages = ``

  statements.forEach(async (statement, i) => {
    messages += `${statement.description}: ${statement.value} \n\n`
  });

  if (!messages) messages = `Você não possui extrato!`

  await stepContext.context.sendActivity(messages)
}

module.exports.AccountDialog = AccountDialog;