const { InputHints, MessageFactory } = require('botbuilder');
const { ConfirmPrompt, ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { models } = require('../models/index')
const moment = require('moment')
const { Op } = require("sequelize");

const TEXT_PROMPT = 'TextPrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
const MENU_CREDIT =
  'Navegue através do menu digitando os seguintes números:\n\n' +
  '1 - Melhor data para compras\n\n' +
  '2 - Limite\n\n' +
  '3 - Saldo\n\n' +
  '4 - Fatura\n\n' +
  '5 - Perda de Cartão\n\n' +
  '6 - Cartão Adicional\n\n' +
  '0 - Menu Principal\n\n'

class CreditCardDialog extends ComponentDialog {
  constructor() {
    super('CreditCardDialog');

    this
      .addDialog(new TextPrompt(TEXT_PROMPT))
      .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
        this.introStep.bind(this),
        this.actStep.bind(this),
        this.bestDay.bind(this),
        this.limit.bind(this),
        this.balance.bind(this),
        this.bill.bind(this),
        this.blockCredit.bind(this),
        this.aditional.bind(this),
        this.finalStep.bind(this),
      ]));

    this.initialDialogId = WATERFALL_DIALOG;
  }

  async introStep(stepContext) {
    const menuDetails = stepContext.options;
    if (!this.userId) this.userId = menuDetails.userId

    const account = await models.accounts.findOne({
      where: {
        userId: this.userId
      }
    })

    const creditCard = await models.credit_cards.findOne({
      where: {
        accountId: account.id,
        aditionalCardId: null
      }
    })

    stepContext.options.creditCard = creditCard

    if (!menuDetails.secondChoice) {
      const msg = MessageFactory.text(MENU_CREDIT, MENU_CREDIT, InputHints.ExpectingInput);
      return await stepContext.prompt(TEXT_PROMPT, { prompt: msg });
    }

    return await stepContext.next();
  }

  async actStep(stepContext) {
    const menuDetails = stepContext.options;
    menuDetails.secondChoice = stepContext.result

    const num = menuDetails.secondChoice;

    if (isNaN(num) || (parseInt(num) > 6 || parseInt(num) < 0)) {
      await stepContext.context.sendActivity('Escolha um número válido!')
    }

    return await stepContext.next(menuDetails)
  }

  async bestDay(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "1") {
      const message = `A melhor data para compras é o dia ${(parseInt(menuDetails.creditCard.expirationDate) - 9) || 1 } de cada mês`
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async limit(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "2") {
      const message = `O seu limite é ${menuDetails.creditCard.limit}`
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async balance(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "3") {
      const message = `O seu limite restante é ${menuDetails.creditCard.balance}`
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async bill(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "4") {
      const salesCredit = await models.sales_credit.findAll({ where: { creditCardId: menuDetails.creditCard.id, isPaid: false } })
      let message = ``
      if (!!salesCredit.length) {
        message = `Essa é a sua fatura: \n\n`
        salesCredit.forEach((sale, i) => {
          message += `${sale.description}: ${sale.value} \n\n`
        })

      } else {
        message = `Você não possui fatura em aberto!`
      }
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async blockCredit(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "5") {
      let message = ``
      if (menuDetails.creditCard.isActive) {
        await models.credit_cards.update({ isActive: false }, { where: { id: menuDetails.creditCard.id } })
        message = `O seu cartão foi bloquado com sucesso!`
      } else {
        message = `O seu cartão já está bloqueado!`
      }
      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async aditional(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "6") {
      const creditCards = await models.credit_cards.findAll({
        where: {
          aditionalCardId: menuDetails.creditCard.id
        }
      })
      const n = creditCards.length
      const message = !!n ?
        `Você possui ${n} cart${n > 1 ? 'ões': 'ão'} adiciona${n > 1 ? 'is': 'l'}!` :
        `Você não possui cartão adicional!`

      await stepContext.context.sendActivity(message)
    }

    return await stepContext.next(menuDetails)
  }

  async finalStep(stepContext) {
    const menuDetails = stepContext.options;

    if (menuDetails.secondChoice === "0") {
      return await stepContext.beginDialog('MainDialog', {})
    }

    return await stepContext.replaceDialog(this.initialDialogId)
  }

}

module.exports.CreditCardDialog = CreditCardDialog;