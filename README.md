# Neppo Bank

Chatbot for neppo bank customer service.
The EER diagram is in the documentation directory, where you can also find instructions.

## Installation

Use the package manager [npm](https://docs.npmjs.com/).

```bash
npm install
```

## Usage

execute:
```bash
npm start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
